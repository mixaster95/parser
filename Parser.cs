﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DiplomaOrganisationParser
{
    class Organisation
    {
        public int article_id { get; set; }
        public string name { get; set; }
    }

    static class Parser
    {
        static string[] organisationSeparators = { "and", " and", ";", " ;", "."};

        static string[] organisationKeyWords = { "university", "universitat", "univ", "hochschule", "college", "institut", "institute", "instituto", "istituto", "hospital", "hopital", "laboratory", "center", "centre", "centro", "academy",  "school", "league", "blood service", "clinic ", "clinic",  "ecole", "alliance", "company", "ministry", "organisation", "gmbh3", " llc", " ltd", "corporation", "unit", "division", "faculty", "department"};

        static string[] deleteKeyWords = { "road", /*"marg",*/ "post box", "allee", "campus", "street", "boulevard", "province", "park", "district", "area", "avda", "sector", "extension",  /*"division", "faculty", "department",*/ "plateforme" };

        public static void ProcessOrganisations(string folderPath)
        {
            var resultList = new List<PreparedOrganisation>();

            //var rawOrganisations = JsonConvert.DeserializeObject<Organisation[]>(File.ReadAllText(folderPath + "affiliations_2014.txt").ToLower());
            var orgs = new List<string>();
            orgs.AddRange(File.ReadAllLines(folderPath + "test.txt"));
            orgs.AddRange(File.ReadAllLines(folderPath + "test2.txt"));
            var countries = File.ReadAllLines(folderPath + "countries.txt");

            //var testResult = new List<string>();
            //var testResult2 = new List<string>();
            //var counter = 1;
            //foreach (var org in rawOrganisations)
            //{
            //    Console.WriteLine("Processing...{0} of {1}", counter++, rawOrganisations.Length);

            //    var containsCountry = false;
            //    var temp = org.name.ToLower();
            //    foreach (var country in countries)
            //    {
            //        var separators = CountryWithSeparators(country, organisationSeparators);
            //        foreach (var separator in separators)
            //        {
            //            while (true)
            //            {
            //                var index = temp.IndexOf(separator);
            //                if (index != -1)
            //                {
            //                    testResult.Add(org.article_id + ":" + temp.Substring(0, index + separator.Length));
            //                    temp = temp.Remove(0, index + separator.Length);
            //                    containsCountry = true;
            //                }
            //                else break;
            //            }
            //        }
            //    }

            //    if (!containsCountry)
            //    {
            //        if (!(temp.StartsWith("email") || temp.StartsWith("e-mail")))
            //        {
            //            testResult2.Add(org.article_id + ":" + temp);
            //        }
            //    }

            var counter = 1;
            foreach (var org in orgs)
            {
                var result = new PreparedOrganisation();
                Console.WriteLine("Processing...{0} of {1}", counter++, orgs.Count);
                var prepString = org.Replace(";", ",").Replace(":", ",").Replace(".", ",").Split(',').ToList();
                result.Id = int.Parse(prepString[0]);
                var preparedString = new List<string>(prepString);
                foreach (var delWord in deleteKeyWords)
                {
                    foreach (var item in prepString)
                    {
                        if (!string.IsNullOrEmpty(item))
                        {
                            if (item.Contains(delWord) && !item.Contains("unversity"))
                            {
                                preparedString.Remove(item);
                            }
                        }
                    }                 
                }
                var containWord = false;
                foreach (var keyWord in organisationKeyWords)
                {
                    if (containWord)
                    {
                        break;
                    }
                    foreach (var item in preparedString)
                    {
                        if (!string.IsNullOrEmpty(item))
                        {
                            if (item.Contains(keyWord))
                            {
                                result.Name = item;
                                containWord = true;
                                break;
                            }
                        }
                    }
                }

                for (int i = 1; i < preparedString.Count; i++)
                {
                    if (!string.IsNullOrEmpty(preparedString[i]))
                    {

                        foreach (var country in countries)
                        {
                            if (preparedString[i].Contains(country))
                            {
                                result.Country = country;
                                break;
                            }
                        }
                    }
                }
                if (result.Name == null)
                {
                    //здесь обработчик для нестандартных имён
                    result.Name = org;
                }
                if (result.Name != null && result.Country != null)
                {
                    if (result.Name.Trim() != result.Country.Trim())
                    {
                        resultList.Add(result);
                    }
                }
                else { resultList.Add(result); }
            }
        //File.WriteAllLines(folderPath + "test.txt", testResult);
        //    File.WriteAllLines(folderPath + "test2.txt", testResult2);
            File.WriteAllText(folderPath + "result.txt", JsonConvert.SerializeObject(resultList, Formatting.Indented));
        }

        //public static void ProcessOrganisations2(string folderPath)
        //{
        //    var resultList = new List<Organisation>();
        //    var stringResultList = new List<string>();

        //    var rawOrganisations = File.ReadAllLines(folderPath + "PubMed.txt");
        //    var countries = File.ReadAllLines(folderPath + "countries.txt");
        //    var countriesAndCities = JsonConvert.DeserializeObject<Dictionary<string, string[]>>(File.ReadAllText(folderPath + "Counties&Cities.txt").ToLower());

        //    var separatedOrganisations = OrganisationsSeparatedByCountry(rawOrganisations, countries).ToArray();
        //    //var separatedOrganisations = File.ReadAllLines(folderPath + "separatedOrganisations.txt");
        //    var counter = 1;

        //    foreach (var organisation in separatedOrganisations)
        //    {
        //        Console.WriteLine("Processing...{0} of {1}", counter++, separatedOrganisations.Length);
        //        var result = new Organisation();
        //        var stringResult = "";

        //        var preparedString = organisation.Replace(";", ".").Split(',').ToList();

        //        result.Id = int.Parse(preparedString[0]);
        //        preparedString.RemoveAt(0);

        //        for (int i = 0; i < preparedString.Count; i++)
        //        {
        //            var index1 = preparedString[i].IndexOf("electronic address:");
        //            if (index1 != -1)
        //            {
        //                preparedString[i] = preparedString[i].Remove(index1, preparedString[i].Length - index1);
        //            }
        //        }              

        //        var countryForCity = "";
        //        foreach (var country in countries)
        //        {
        //            if (preparedString.Count() > 0)
        //            {
        //                if (preparedString[preparedString.Count() - 1].Replace(" ","").Contains(country + ".") || preparedString[preparedString.Count() - 1].Trim() == country)
        //                {
        //                    preparedString.RemoveAt(preparedString.Count() - 1);

        //                    if (country == "uk")
        //                    {
        //                        countryForCity = "united kingdom";
        //                    }
        //                    if (country == "korea")
        //                    {
        //                        countryForCity = "republic of korea";
        //                    }
        //                    else countryForCity = country;
        //                    break;
        //                }
        //            }
        //        }

        //        var pattern = @"([\w\.\-]+)@([\w\-]+)(\.\w+)(\.\w+)*";
        //        if (preparedString.Count() > 0)
        //        {
        //            if (Regex.IsMatch(preparedString[preparedString.Count() - 1], pattern))
        //            {
        //                foreach (var country in countries)
        //                {
        //                    if (preparedString[preparedString.Count() - 1].Replace(" ", "").Contains(country))
        //                    {
        //                        if (country == "uk")
        //                        {
        //                            countryForCity = "united kingdom";
        //                        }
        //                        if (country == "korea")
        //                        {
        //                            countryForCity = "republic of korea";
        //                        }
        //                        else countryForCity = country;
        //                        break;
        //                    }
        //                }
        //                preparedString.RemoveAt(preparedString.Count() - 1);
        //            }
        //        }

        //        for (int i = 0; i < preparedString.Count(); i++)
        //        {
        //            var numCounter = 0;
        //            foreach (var sign in preparedString[i])
        //            {
        //                if (char.IsDigit(sign) || char.IsWhiteSpace(sign))
        //                {
        //                    numCounter++;
        //                }
        //            }
        //            if (numCounter == preparedString[i].Length)
        //            {
        //                preparedString.RemoveAt(i);
        //                i--;
        //                continue;
        //            }
        //            var digitCounter = 0;
        //            foreach (var sign in preparedString[i])
        //            {
        //                if (char.IsDigit(sign))
        //                {
        //                    digitCounter++;
        //                }
        //                if (digitCounter > 2)
        //                {
        //                    preparedString.RemoveAt(i);
        //                    i--;
        //                    break;
        //                }
        //            }
        //        }

        //        for (int i = 0; i < preparedString.Count(); i++)
        //        {
        //            try
        //            {
        //                foreach (var city in countriesAndCities[countryForCity])
        //                {
        //                    if (preparedString[i].Trim() == city)
        //                    {
        //                        for (int j = i; j < preparedString.Count(); j++)
        //                        {
        //                            preparedString.RemoveAt(j);
        //                            j--;
        //                        }                                
        //                        break;
        //                    }
        //                    var index = preparedString[i].IndexOf(city);
        //                    if (index != -1)
        //                    {
        //                        for (int j = index + city.Length; j < preparedString[i].Length; j++)
        //                        {
        //                            if (char.IsDigit(preparedString[i][j]))
        //                            {
        //                                preparedString.RemoveAt(i);
        //                                i--;
        //                                break;
        //                            }
        //                        }
        //                        break;
        //                    }
        //                }
        //            }
        //            catch
        //            {
        //                foreach (var city in countriesAndCities["usa"])
        //                {
        //                    if (preparedString[i].Trim() == city)
        //                    {
        //                        for (int j = i; j < preparedString.Count(); j++)
        //                        {
        //                            preparedString.RemoveAt(j);
        //                            j--;
        //                        }
        //                        break;
        //                    }
        //                    var index = preparedString[i].IndexOf(city);
        //                    if (index != -1)
        //                    {
        //                        for (int j = index + city.Length; j < preparedString[i].Length; j++)
        //                        {
        //                            if (char.IsDigit(preparedString[i][j]))
        //                            {
        //                                preparedString.RemoveAt(i);
        //                                i--;
        //                                break;
        //                            }
        //                        }
        //                        break;
        //                    }
        //                }
        //            }
        //        }
        //        if (preparedString.Count() > 1)
        //        {
        //            foreach (var delete in deleteKeyWords)
        //            {
        //                for (int i = 0; i < preparedString.Count(); i++)
        //                {
        //                    if (preparedString[i].Contains(delete))
        //                    {
        //                        preparedString.RemoveAt(i);
        //                        i--;
        //                    }
        //                    if (preparedString.Count() == 1) break;
        //                }
        //            }
        //        }


        //        if (preparedString.Count() > 0)
        //        {
        //            for (int i = 0; i < preparedString.Count() - 1; i++)
        //            {
        //                stringResult += preparedString[i].Trim() + ",";
        //            }
        //            stringResult += preparedString[preparedString.Count() - 1].Trim();
        //        }
        //        result.Country = countryForCity;
        //        result.Name = stringResult;
        //        resultList.Add(result);
        //        //stringResultList.Add(stringResult);
        //    }

        //    File.WriteAllText(folderPath + "BigResult.txt", JsonConvert.SerializeObject(resultList, Formatting.Indented));
        //    //File.WriteAllLines(folderPath + "result2.txt", stringResultList);
        //    //File.WriteAllText(folderPath + "result2.txt", JsonConvert.SerializeObject(resultList, Formatting.Indented));
        //}

        private static List<string> OrganisationSeparatedByCountry(string input, string[] countries)
        {
            var result = new List<string>();
            
            
            return result;
        }

        private static List<string> CountryWithSeparators(string country, string[] separators)
        {
            var result = new List<string>();

            //result.Add(", " + country + " ");
            //result.Add(",  " + country + " ");
            result.Add(", " + country + ",");
            result.Add("," + country + ".");
            result.Add("(" + country + ")");

            foreach (var separator in separators)
            {               
                result.Add(" " + country + separator);
                result.Add(", " + country + separator);
                result.Add(",  " + country + separator);
            }

            return result;
        }
    }
}
